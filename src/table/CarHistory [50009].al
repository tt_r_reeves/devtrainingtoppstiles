table 50009 "CarHistory"
{
    Caption = 'CarHistory';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.';
            DataClassification = ToBeClassified;
            AutoIncrement = true;
        }
        field(2; "Car Entry No."; Integer)
        {
            Caption = 'Car Entry No.';
            DataClassification = ToBeClassified;
            TableRelation = Car."Entry No.";
        }
        field(3; Activity; Option)
        {
            Caption = 'Activity';
            DataClassification = ToBeClassified;
            OptionMembers = "",MOT,Service,Repair,Sale,Other;
        }
        field(4; "Activity Date"; Date)
        {
            Caption = 'Activity Date';
            DataClassification = ToBeClassified;
        }
        field(5; Notes; Text[250])
        {
            Caption = 'Notes';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
        key(SK; "Activity Date")
        {
            Clustered = false;
        }
    }

}
