table 50006 "Brand"
{
    DataClassification = ToBeClassified;
    LookupPageId = BrandList;


    fields
    {
        field(1; "Brand Code"; Code[20])
        {
            DataClassification = ToBeClassified;

        }
        field(2; "Name"; Text[50])
        {
            DataClassification = ToBeClassified;

        }
    }

    keys
    {
        key(PK; "Brand Code")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}