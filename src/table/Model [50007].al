table 50007 "Model"
{
    Caption = 'Model';
    DataClassification = ToBeClassified;
    LookupPageId = ModelList;

    fields
    {
        field(1; "Brand Code"; Code[20])
        {
            Caption = 'Brand Code';
            DataClassification = ToBeClassified;
            TableRelation = Brand;
        }
        field(2; "Model Code"; Code[20])
        {
            Caption = 'Model Code';
            DataClassification = ToBeClassified;
        }
        field(3; Name; Text[50])
        {
            Caption = 'Name';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Brand Code", "Model Code")
        {
            Clustered = true;
        }
    }

}
