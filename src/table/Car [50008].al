table 50008 "Car"
{
    Caption = 'Car';
    DataClassification = ToBeClassified;
    LookupPageId = CarList;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.';
            DataClassification = ToBeClassified;
            AutoIncrement = true;
            Editable = false;
        }
        field(2; "Brand Code"; Code[20])
        {
            Caption = 'Brand Code';
            DataClassification = ToBeClassified;
            TableRelation = Brand;
            trigger OnValidate()
            var
            //no local variables
            begin
                Rec.Validate("Model Code", '');
            end;
        }
        field(3; "Model Code"; Code[20])
        {
            Caption = 'Model Code';
            DataClassification = ToBeClassified;
            TableRelation = Model."Model Code" where("Brand Code" = field("Brand Code"));
        }
        field(4; "Registration No."; Text[20])
        {
            Caption = 'Registration No.';
            DataClassification = ToBeClassified;
        }
        field(5; Year; Integer)
        {
            Caption = 'Year';
            DataClassification = ToBeClassified;
        }
        field(6; "Engine Type"; Option)
        {
            Caption = 'Engine Type';
            DataClassification = ToBeClassified;
            OptionMembers = " ",Petrol,Diesel,Hybrid,Electric;
        }
        field(7; "Engine Size"; Decimal)
        {
            Caption = 'Engine Size';
            DataClassification = ToBeClassified;
        }
        field(8; Colour; Text[20])
        {
            Caption = 'Colour';
            DataClassification = ToBeClassified;
        }
        field(9; Notes; Text[250])
        {
            Caption = 'Notes';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }
    trigger OnDelete()
    var
        CarHistory: Record CarHistory;
    begin
        CarHistory.Reset();
        CarHistory.SetRange("Car Entry No.", rec."Entry No.");
        CarHistory.DeleteAll(true);
    end;

}
