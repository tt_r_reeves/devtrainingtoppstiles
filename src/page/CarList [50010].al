page 50010 "CarList"
{

    PageType = List;
    SourceTable = Car;
    Caption = 'CarList';
    ApplicationArea = All;
    UsageCategory = Lists;
    Editable = false;
    CardPageId = CarCard;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Brand Code"; "Brand Code")
                {
                    ApplicationArea = All;
                }
                field("Engine Size"; "Engine Size")
                {
                    ApplicationArea = All;
                }
                field("Engine Type"; "Engine Type")
                {
                    ApplicationArea = All;
                }
                field("Entry No."; "Entry No.")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("Model Code"; "Model Code")
                {
                    ApplicationArea = All;
                }
                field("Registration No."; "Registration No.")
                {
                    ApplicationArea = All;
                }
                field(Colour; Colour)
                {
                    ApplicationArea = All;
                }
                field(Notes; Notes)
                {
                    ApplicationArea = All;
                }
                field(Year; Year)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
