page 50013 "Car Stuff"
{

    PageType = List;
    SourceTable = Car;
    Caption = 'Car Stuff';
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Engine Size"; "Engine Size")
                {
                    ApplicationArea = All;
                }
                field("Registration No."; "Registration No.")
                {
                    ApplicationArea = All;
                }
                field("Model Code"; "Model Code")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}
