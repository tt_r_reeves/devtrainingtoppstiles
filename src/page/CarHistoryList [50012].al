page 50012 "CarHistoryList"
{

    PageType = ListPart;
    SourceTable = CarHistory;
    Caption = 'CarHistoryList';
    PopulateAllFields = true;
    SourceTableView = sorting("Activity Date");

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Activity Date"; "Activity Date")
                {
                    ApplicationArea = All;
                }
                field(Activity; Activity)
                {
                    ApplicationArea = All;
                }
                field(Notes; Notes)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
