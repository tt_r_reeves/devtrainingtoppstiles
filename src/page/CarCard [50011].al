page 50011 "CarCard"
{

    PageType = Card;
    SourceTable = Car;
    Caption = 'CarCard';

    layout
    {
        area(content)
        {
            group(General)
            {
                field("Brand Code"; "Brand Code")
                {
                    ApplicationArea = All;
                }
                field("Model Code"; "Model Code")
                {
                    ApplicationArea = All;
                }
                field("Registration No."; "Registration No.")
                {
                    ApplicationArea = All;
                }
                field("Entry No."; "Entry No.")
                {
                    ApplicationArea = All;
                }
            }
            group(Details)
            {
                field("Engine Type"; "Engine Type")
                {
                    ApplicationArea = All;
                }
                field("Engine Size"; "Engine Size")
                {
                    ApplicationArea = All;
                }
                field(Colour; Colour)
                {
                    ApplicationArea = All;
                }
                field(Year; Year)
                {
                    ApplicationArea = All;
                }
                field(Notes; Notes)
                {
                    ApplicationArea = All;
                }
            }
            group(history)
            {
                part(CarHistory; CarHistoryList)
                {
                    ApplicationArea = All;
                    SubPageLink = "Car Entry No." = field("Entry No.");
                }
            }
        }
    }
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        if rec.Find() then begin
            if ("Brand Code" <> '') and ("Model Code" = '') then begin
                Error('You must enter a Model Code');
            end;
        end;

    end;
}
