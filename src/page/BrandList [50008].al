page 50008 "BrandList"
{

    PageType = List;
    SourceTable = Brand;
    Caption = 'BrandList';
    ApplicationArea = All;
    UsageCategory = Lists;
    Editable = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Brand Code"; "Brand Code")
                {
                    ApplicationArea = All;
                }
                field(Name; Name)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ShowModelList)
            {
                ApplicationArea = All;
                Caption = 'Model List';
                RunObject = page ModelList;
                RunPageLink = "Brand Code" = field("Brand Code");
                Promoted = true;
                PromotedCategory = Process;
                Image = ListPage;
            }
        }
    }

}
