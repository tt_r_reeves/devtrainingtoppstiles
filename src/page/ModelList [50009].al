page 50009 "ModelList"
{

    PageType = List;
    SourceTable = Model;
    Caption = 'ModelList';
    ApplicationArea = All;
    UsageCategory = Lists;
    Editable = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Brand Code"; "Brand Code")
                {
                    ApplicationArea = All;
                }
                field("Model Code"; "Model Code")
                {
                    ApplicationArea = All;
                }
                field(Name; Name)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(ShowCarList)
            {
                ApplicationArea = All;
                Caption = 'Car List';
                RunObject = page CarList;
                RunPageLink = "Brand Code" = field("Brand Code"), "Model Code" = field("Model Code");
                Promoted = true;
                PromotedCategory = Process;
                Image = ListPage;
            }
        }
    }

}
